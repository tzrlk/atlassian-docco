/*!! 1. Getting Started, 2. Awesome */
/*!
##The Test Header

This is a basic header.

If you put a docco comment BEFORE ANY CODE in a file, it is treated as the header.

*neat, huh?*

we can also use a basePath variable for [linking to a local file](${basePath}/src/test/resources/simple.html.html)
*/

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.atlassian.docco.Docco;
import com.atlassian.docco.DoccoBatch;

import org.junit.Test;
import org.parboiled.common.FileUtils;

/*! this is a docco comment */
public class JavaFile
{
    /*!#start
    This is a multi-line docco comment.

    woo hoo
    */
    @Test
    public void simpleJava() throws IOException, URISyntaxException
    {
        URL url = this.getClass().getResource("src/test/java/com/atlassian/docco/SmokeTest.java");
        File me = new File(url.getFile());

        System.out.println("me = " + me);
        File target = new File(new File(""), "target");
        File htmlFile = new File(target, "PegdownTest.html");

        Docco docco = Docco.builder().build();

        docco.writeHtml(me,htmlFile);

    }

    /*!-helper methods
    These are some things that help you!
    ###Seriously!
    */
    @Test
    public void simpleHtml() throws IOException, URISyntaxException
    {

        File me = new File(new File(""), "atlassian-docco/src/test/resources/simple.html");
        File target = new File(new File(""), "target");
        File htmlFile = new File(target, "HTMLTest.html");

        Docco docco = Docco.builder().build();

        docco.writeHtml(me,htmlFile);

    }

    /*! just another test */
    @Test
    public void tryABatch() throws Exception
    {
        File base = new File(new File(""), "atlassian-docco/");
        File target = new File(new File(""), "target/batch");
        FileUtils.forceMkdir(target);
        DoccoBatch batch = DoccoBatch.builder(base,target).build();

        batch.generateDocco();
    }
}
