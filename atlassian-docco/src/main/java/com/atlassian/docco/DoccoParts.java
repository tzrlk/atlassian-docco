/*! docco parts */
package com.atlassian.docco;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;

import org.pegdown.PegDownProcessor;

/**
 * @since 1.0
 */
public class DoccoParts
{
    private final Header header;
    private final List<Section> sections;
    private final List<String> groups;

    public DoccoParts()
    {
        this(new PegDownProcessor());
    }

    public DoccoParts(PegDownProcessor pegDown)
    {
        this.header = new Header(pegDown);
        this.sections = new ArrayList<Section>();
        this.groups = new ArrayList<String>();
    }

    public void addSection(Section section)
    {
        this.sections.add(section);
    }

    public List<Section> getSections()
    {
        return ImmutableList.copyOf(sections);
    }

    public void addGroup(String group)
    {
        this.groups.add(group);
    }

    public List<String> getGroups()
    {
        return ImmutableList.copyOf(groups);
    }

    public Header getHeader()
    {
        return header;
    }
}
