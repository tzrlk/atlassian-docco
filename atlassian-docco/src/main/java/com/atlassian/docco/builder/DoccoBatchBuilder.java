package com.atlassian.docco.builder;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.docco.Docco;
import com.atlassian.docco.DoccoBatch;
import com.atlassian.docco.mapping.DoccoFileMappingManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * @since 1.0
 */
public class DoccoBatchBuilder
{
    private File basePath;
    private File outputPath;
    private String[] excludes;
    private String[] includes;
    private URL hSoyTemplate;
    private URL vSoyTemplate;
    private DoccoFileMappingManager fileMappings;
    private String title;
    private String indexFilename;
    private List<File> customResources;
    private Boolean stripJavadoc;
    private Boolean skipNoDocco;

    public DoccoBatchBuilder(File basePath, File outputPath)
    {
        this.basePath = basePath;
        this.outputPath = outputPath;
        this.excludes = new String[0];
        this.includes = new String[0];
    }

    public DoccoBatchBuilder horizontalTemplate(File soyTemplate)
    {
        checkState(soyTemplate.exists(),"Horizontal Soy template doesn't exist!");
        try
        {
            this.hSoyTemplate = soyTemplate.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return this;
    }

    public DoccoBatchBuilder verticalTemplate(File soyTemplate)
    {
        checkState(soyTemplate.exists(),"Vertical Soy template doesn't exist!");
        try
        {
            this.vSoyTemplate = soyTemplate.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return this;
    }

    public DoccoBatchBuilder excludes(String[] excludes)
    {
        this.excludes = excludes;
        return this;
    }

    public DoccoBatchBuilder includes(String[] includes)
    {
        this.includes = includes;
        return this;
    }

    public DoccoBatchBuilder fileMappings(DoccoFileMappingManager fileMappings)
    {
        this.fileMappings = fileMappings;
        return this;
    }

    public DoccoBatchBuilder indexTitle(String title)
    {
        this.title = title;
        return this;
    }

    public DoccoBatchBuilder indexFilename(String indexFilename)
    {
        this.indexFilename = indexFilename;
        return this;
    }

    public DoccoBatchBuilder resources(List<File> customResources)
    {
        this.customResources = customResources;
        return this;
    }

    public DoccoBatchBuilder stripJavadoc(Boolean stripJavadoc)
    {
        this.stripJavadoc = stripJavadoc;
        return this;
    }

    public DoccoBatchBuilder skipNoDocco(Boolean skipNoDocco)
    {
        this.skipNoDocco = skipNoDocco;
        return this;
    }

    public DoccoBatch build()
    {
        checkNotNull(basePath);
        checkState(basePath.exists(),"Base Path doesn't exist!");

        checkNotNull(outputPath);
        if(!outputPath.exists())
        {
            try
            {
                FileUtils.forceMkdir(outputPath);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new IllegalStateException("Error creating docco output directory!", e);
            }
        }

        if(null == hSoyTemplate)
        {
            try
            {
                hSoyTemplate = Docco.getDefaultHorizontalTemplate();
            }
            catch (URISyntaxException e)
            {
                throw new IllegalStateException("Unable to load default horizontal soy template!", e);
            }
        }

        if(null == vSoyTemplate)
        {
            try
            {
                vSoyTemplate = Docco.getDefaultVerticalTemplate();
            }
            catch (URISyntaxException e)
            {
                throw new IllegalStateException("Unable to load default vertical soy template!", e);
            }
        }

        if(null == fileMappings)
        {
            fileMappings = new DoccoFileMappingManager();
        }

        if(StringUtils.isBlank(title))
        {
            try
            {
                title = basePath.getCanonicalFile().getName() + " index";
            }
            catch (IOException e)
            {
                title = "Index";
            }
        }

        if(StringUtils.isBlank(indexFilename))
        {
            indexFilename = "index.html";
        }

        if(null == customResources)
        {
            customResources = new ArrayList<File>();
        }

        if(null == stripJavadoc)
        {
            stripJavadoc = Boolean.TRUE;
        }

        if(null == skipNoDocco)
        {
            skipNoDocco = Boolean.TRUE;
        }

        Docco docco = Docco.builder().horizontalTemplate(hSoyTemplate).verticalTemplate(vSoyTemplate).fileMappings(fileMappings).stripJavadoc(stripJavadoc).resources(customResources).build();

        return new DoccoBatch(docco,basePath,outputPath,excludes,includes,hSoyTemplate,vSoyTemplate,fileMappings,title,indexFilename,skipNoDocco.booleanValue());
    }
}
